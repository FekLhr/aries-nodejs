'use strict'

module.exports = {
  PORT: 3000,
  uri: 'mongodb://localhost/ARIES',
  pathToUpload: path.join(__dirname, "../../../uploads")
};
