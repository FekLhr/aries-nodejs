'use strict'

const bodyParser = require('body-parser');

module.exports = {
    init:initExpress,
};

function initExpress(app) {
  app.use(bodyParser.urlencoded({extended: false}));

  app.use(bodyParser.json());

  app.use(function (req, res, next) {
    console.log('general midd Express');
    // console.log(req.resources);
    req.resources = req.resources || {};
    return next();
})
};