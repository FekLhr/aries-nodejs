const mongoose = require('mongoose');
const { formattedDate } = require('../helpers/date');
const Schema = mongoose.Schema;

const postSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  post: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true,
    default: formattedDate()
  }
});

module.exports = mongoose.model('Post', postSchema, 'posts');
