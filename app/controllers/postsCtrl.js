'use strict'
const Post = require('../models/postModel');

module.exports = {
  getPosts,
  newPost,
  deletePost,
  updatePost,
};

function updatePost(req, res, next) {
  const { postId } = req.params;
  const updateData = req.body;
  console.log('postID', postId)
  Post.findOneAndUpdate({ _id: postId }, updateData, function(err, result) {
    if(err) {
      console.log('err', err);
      req.resources.users = {test:2};

      return next({message: 'Some specific error here'})
    }
    console.log('result', result);
    req.resources.users = result;
    next();
  })
}

function deletePost(req, res, next) {
  console.log('req.params', req.params);
  const { postId } = req.params;

  Post.deleteOne({_id: postId}, function(err, result) {
    if(err) {
      console.log('err', err)
      return res.send('error from delete user')
    }
    console.log('post ',postId,' deleted.')
    next()
  })
}


function getPosts(req, res, next) {
  console.log('GET Posts');
  Post.find(function(err, result) {
    if(err) {
      console.log('err', err);
      return res.send('Some error from get posts')
    }
    req.resources.posts = result;
    next()
  })
}


function newPost(req, res, next) {
  console.log('req.body',req.body);
  const post = new Post(req.body);
  post.save(function(err, result) {
    if(err) {
      return next(err)
    }
    req.resources.posts = result;
    return next()
  });
}

function midd2(req, res, next) {
  console.log('midd 2');
  next()

}

function midd3(req, res, next) {
  console.log('midd 3');
  res.send('Midd 3')
}
