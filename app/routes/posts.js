'use strict'

const express = require('express');
const router = express.Router();

const postsCtrl = require('../controllers/postsCtrl');
const commonCtrl = require('../controllers/commonCtrl');


router.get('/posts', postsCtrl.getPosts, commonCtrl.responseToJSON('posts'));

// router.get('/users', userCtrl.getUsers, function(req, res, next) {
//   res.json(req.resources['users']);
// });

router.post('/posts', postsCtrl.newPost, commonCtrl.responseToJSON('users'));

router.delete('/posts/:postId', postsCtrl.deletePost, commonCtrl.responseToJSON('posts'));

router.put('/posts/:postId', postsCtrl.updatePost, commonCtrl.responseToJSON('users'));

module.exports = router;
