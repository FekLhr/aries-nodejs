'use strict'

const express = require('express');
const router = express.Router();
const multer = require('multer')
const {pathToUpload} = require('../config')
const userCtrl = require('../controllers/usersCtrl');
const commonCtrl = require('../controllers/commonCtrl');


const storage =multer.diskStorage({
    destination: function (req,file,cb){
        const pathToDestination = pathToUpload;
        cb(null, pathToDestination)
    },
    filename: function(req,file,cb){
        console.log("file",file.mimetype);
        const mimetype = file.mimetype.split('/')[1];
        console.log('mimetype',mimetype);
        cb(null, `${Date.now()}-${file.originalname}`)
    }
});
const upload = multer({storage:storage});


router.post('/upload-profile', upload.single('profilePicture'), function(req, res, next) {
    console.log('FILE', req.file);
    // console.log('FILES', req.files);
    res.send('Uploaded')
})
router.get('/download-image', function(req, res, next){
    const pathToFile = `${pathToUpload}/1598015718634-Prince.jpg`;
    res.download (pathToFile)
})
// router.get('/users', userCtrl.getUsers, function(req, res, next) {
//   res.json(req.resources['users']);
// });
router.get('/admin/:userId',userCtrl.getUserById, userCtrl.verifyAdmin, commonCtrl.responseToJSON('admin'))
router.get('/users', userCtrl.getUsers, commonCtrl.responseToJSON('users'));
router.post('/users', userCtrl.createUsers, commonCtrl.responseToJSON('users'));
router.delete('/users/:userId', userCtrl.getUserById, userCtrl.deleteUser, commonCtrl.responseToJSON('users'));
router.put('/users/:userId', userCtrl.updateUser, commonCtrl.responseToJSON('users'));
module.exports = router;